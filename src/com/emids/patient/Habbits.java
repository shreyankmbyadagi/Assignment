package com.emids.patient;

public class Habbits {
	
	private String smoking;
	private String alchol;
	private String dailyExercise;
	private String drugs;
	
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlchol() {
		return alchol;
	}
	public void setAlchol(String alchol) {
		this.alchol = alchol;
	}
	public String getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	

}
